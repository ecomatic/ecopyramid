<?php
/**
 * Created by PhpStorm.
 * User: Yuki
 * Date: 25/01/2018
 * Time: 11:33
 */

namespace Ecomatic\EcoPyramid\Helper;


use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Belvg\Seller\Model\SellerFactory;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\DirectoryList;
use Belvg\Seller\Helper\Seller;

class Data extends AbstractHelper {

	protected $_sellerFactory;
	protected $_scopeConfig;
	protected $_directory_list;
	protected $_customer_repository;
	protected $_sellerHelper;

	public function __construct(
		Context $context,
		SellerFactory $sellerFactory,
		ScopeConfigInterface $scopeConfig,
		DirectoryList $directory_list,
		CustomerRepositoryInterface $customer_repository,
		Seller $sellerHelper
	) {
		$this->_sellerFactory = $sellerFactory;
		$this->_scopeConfig = $scopeConfig;
		$this->_directory_list = $directory_list;
		$this->_customer_repository = $customer_repository;
		$this->_sellerHelper = $sellerHelper;
		parent::__construct( $context );
	}

	/**
	 * @return bool
	 */
	public function isEnabled() {
		return $this->_scopeConfig->getValue('ecomatic_ecopyramid/general/enable');
	}

	public function exportFeed( $feed, $file, $append ) {
		$pub_dir = $this->getPath( 'pub' );
		if ( ! file_exists( $pub_dir . '/pyramid' ) ) {
			mkdir( $pub_dir . "/pyramid" );
		}
		file_put_contents( $pub_dir . '/pyramid/' . $file, $feed, ( $append ? FILE_APPEND : 0 ) );
	}

	/**
	 * @param $sellerId
	 *
	 * @return \Belvg\Seller\Model\Seller
	 * @throws
	 */
	public function getSeller( $sellerId ) {
		$seller = $this->_sellerFactory->create()->load( $sellerId );

		return $seller;
	}

	public function getSellerData( $sellerId ) {
		return $this->_sellerHelper->getSellerData( $sellerId );
	}

	public function getPath( $code ) {
		try {
			$path = $this->_directory_list->getPath( $code );
		} catch ( FileSystemException $e ) {
			$path = __DIR__ . '/../../../../../' . ( $code == 'log' ? 'var/log' : 'pub' );
			file_put_contents('/../../../../../var/log/pyramid.log', $e->getMessage(), 8);
		}

		return $path;
	}

//	public function poke( $output ) {
//		$log_dir = $this->getPath( 'log' );
//		file_put_contents($log_dir .'/poke', date('m.d h:m') . $output . "\n", 8);
//	}
}