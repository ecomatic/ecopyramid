<?php
/**
 * Created by PhpStorm.
 * User: Yuki
 * Date: 23/01/2018
 * Time: 12:19
 */

namespace Ecomatic\EcoPyramid\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {
	/**
	 *
	 *
	 * @param SchemaSetupInterface $setup
	 * @param ModuleContextInterface $context
	 *
	 * @return void
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	public function install( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
		$installer = $setup;
		$installer->startSetup();

		$orderTable = 'sales_order';
		$setup->getConnection()->addColumn(
			$setup->getTable($orderTable),
			'pyramid_status',
			[
				'type' => Table::TYPE_INTEGER,
				'',
				'default' => "0",
				'nullable' => false,
				'comment' => 'Pyramid Feed Status'
			]
		);

		$installer->endSetup();
	}
}