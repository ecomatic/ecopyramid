<?php
/**
 * Created by PhpStorm.
 * User: Yuki
 * Date: 25/01/2018
 * Time: 11:28
 */

namespace Ecomatic\EcoPyramid\Cron;

use Ecomatic\EcoPyramid\Helper\Data;
use \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use \Magento\Catalog\Model\Product\Attribute\Source\Status;
use \Magento\Catalog\Model\Product\Visibility;


class ArticleCron {

	protected $_helper;
	protected $_collection_factory;
	protected $_status;
	protected $_visibility;

	public function __construct(
		Data $helper,
		CollectionFactory $collection_factory,
		Status $status,
		Visibility $visibility
	) {
		$this->_helper = $helper;
		$this->_collection_factory = $collection_factory;
		$this->_status = $status;
		$this->_visibility = $visibility;
	}

	public function execute() {
		if ( ! $this->_helper->isEnabled() ) {
			return $this;
		}

		$productCollection = $this->_collection_factory->create();

		$productCollection
			->addAttributeToFilter( 'status', [ 'in' => $this->_status->getVisibleStatusIds() ] )
			->addAttributeToFilter( 'visibility', [ 'in' => $this->_visibility->getVisibleInSiteIds() ] )
			->addAttributeToFilter( 'type_id', [ 'neq' => 'configurable' ] )
			->addAttributeToSelect( '*' );


		$str = '';
		foreach ( $productCollection as $product ) {
			$str .= $this->_getProductString( $product );
		}

		$this->_helper->exportFeed( $str, 'article/pyramid_article_feed.csv', false );

		return $this;
	}

	/**
	 * @param $product \Magento\Catalog\Model\Product
	 *
	 * @return string
	 */
	protected function _getProductString( $product ) {
		$str = $product->getSku() . ';'; //Artikelkod
		$str .= $product->getName() . ';'; //Artikelnamn
		$str .= $this->getArticleType($product) . ';'; //Artikeltyp
		$str .= $product->getPriceInfo()->getPrice('final_price')->getAmount()->getBaseAmount(). ';'; //Pris
		$str .= "\n";
		return $str;
	}

	/**
	 * @param $product \Magento\Catalog\Model\Product
	 *
	 * @return null
	 */
	protected function getArticleType( $product ) {
		$data = $product->getData('artikeltyp');
		$data = $data ? $data : 'A1';
		return $data;
	}
}