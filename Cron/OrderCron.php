<?php
/**
 * Created by PhpStorm.
 * User: Yuki
 * Date: 23/01/2018
 * Time: 12:28
 */

namespace Ecomatic\EcoPyramid\Cron;

use Ecomatic\EcoPyramid\Helper\Data;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use \Magento\Sales\Model\ResourceModel\Order;


class OrderCron {

	protected $_helper;
	protected $_collection_factory;
	protected $_order;

	public function __construct(
		Data $helper,
		CollectionFactory $collection_factory,
		Order $order
	) {
		$this->_helper = $helper;
		$this->_collection_factory = $collection_factory;
		$this->_order = $order;
	}

	public function execute() {
		if ( !$this->_helper->isEnabled() ) {
			return $this;
		}

		$orderCollection = $this->_collection_factory->create()
		                                             ->addAttributeToSelect('*')
		                                             ->addFieldToFilter('pyramid_status', ['eq' => 0])
		                                             ->addFieldToFilter(['status', 'state'], [ ['eq' => 'complete'], ['eq' => 'complete'] ]);

		$str = '';
		/** @var $order \Magento\Sales\Model\Order */
		foreach ( $orderCollection as $order ) {
				$str .= $this->_getOrderString( $order );
		}

		$this->_helper->exportFeed($str, 'order/pyramid_order_feed.txt', true);

		return $this;
	}

	/**
	 * @param $order \Magento\Sales\Model\Order
	 *
	 * @return string
	 */
	protected function _getOrderString( $order ) {
		$log_dir = $this->_helper->getPath( 'log' );

		if ( $order && $order->getSellerId() ) {
			$sellerData = $this->_helper->getSellerData( $order->getSellerId() );

			$str = '01;';
			$str .= $order->getCustomerId() . ';'; // Kundkod
			$str .= 'B' . $order->getIncrementId() . ';'; //Ordernr
			$str .= $order->getCustomerName() . ';'; //Namn

			$billingAddress = $order->getBillingAddress();
			$str            .= $this->_formatStreetAddress( $billingAddress ) . ';'; // Bil. adress 1
			$str            .= $billingAddress->getPostcode() . ' ' . $billingAddress->getCity() . ';'; //Bil. adress 2
			$str            .= $billingAddress->getTelephone() . ';';
			$str            .= $order->getCustomerEmail() . ';';

			$shippingAddress = $order->getShippingAddress();
			$str             .= $shippingAddress->getName() . ';';
			$str             .= $this->_formatStreetAddress( $shippingAddress ) . ';'; //Lev. adress 1
			$str             .= $shippingAddress->getPostcode() . ' ' . $shippingAddress->getCity() . ';'; //Lev. adress 2
			$str             .= $shippingAddress->getTelephone() . ';';

			$str .= $order->getIncrementId() . ';'; //Ordernr
			$str .= 'FL' . $sellerData['seller_id'] . ';'; //Floristkod
			$str .= $sellerData['store_name'] . ';'; //Företagsnamn

			$items                 = $order->getAllItems();
			$bundlesWithFixedPrice = array();
			foreach ( $items as $item ) {
				if ( $item->getProductType() == 'configurable' ) {
					continue;
				} elseif ( in_array( $item->getParentItemId(), $bundlesWithFixedPrice ) ) {
					continue;
				} elseif ( $item->getProductType() == 'bundle' ) {
					$product = $item->getProduct();
					if ( $product->getPriceType() == \Magento\Bundle\Model\Product\Price::PRICE_TYPE_FIXED ) {
						$bundlesWithFixedPrice[] = $item->getItemId();
					} elseif ( $product->getPriceType() == \Magento\Bundle\Model\Product\Price::PRICE_TYPE_DYNAMIC ) {
						continue;
					}
				}

				$str .= "\n";
				$str .= $this->_getProductString( $item );
			}
			$str .= "\n";

			$str .= '11;FRAKT;' . $order->getShippingDescription() . ';1;' . $order->getShippingAmount() . "\n";

			try {
				$order->setData( 'pyramid_status', 1 );
				$this->_order->save( $order );
			} catch ( \Exception $e ) {
				file_put_contents( $log_dir . '/pyramid.log', $e->getMessage() . "\n", FILE_APPEND );
			}

			return $str;
		}

		try {
			$order->setData( 'pyramid_status', 1 );
			$this->_order->save( $order );
		} catch ( \Exception $e ) {
			file_put_contents( $log_dir . '/pyramid.log', $e->getMessage() . "\n", FILE_APPEND );
		}
		file_put_contents( $log_dir . '/pyramid.log', "Order " . $order->getIncrementId() . " has no seller\n", FILE_APPEND );

		return '';
	}

	/**
	 * @param $item \Magento\Sales\Model\Order\Item
	 *
	 * @return string
	 */
	protected function _getProductString( $item ) {
		$str = '11;';

		$str .= $item->getSku() . ';'; //Artikelkod
		$str .= $item->getName() . ';'; //Benämning
		$item = $item->getParentItem() ? $item->getParentItem() : $item;
		$str .= $item->getQtyOrdered() . ';'; //Antal
		$str .= $item->getPrice() . ';'; //Pris

		return $str;
	}

	/**
	 * @param \Magento\Sales\Api\Data\OrderAddressInterface $address
	 *
	 * @return string
	 */
	protected function _formatStreetAddress( $address ) {
		$street = $address->getStreet();
		$s0     = '';
		if ( array_key_exists( 0, $street ) ) {
			$s0 = $street[0];
		}
		if ( array_key_exists( 1, $street ) ) {
			$s1 = $street[1];
		}
		return $s0 . ( isset( $s1 ) ? ', ' . $s1 : '' );
	}
}